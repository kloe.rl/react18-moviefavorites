import PropTypes from "prop-types";
import { useState } from 'react';

import "./MovieItem.css";

function MovieItem(props) {
  const { title, released, director, poster } = props;

  // Fetch the value stored id localStorage
const currentIsFavoriteValue = localStorage.getItem("FavoriteState") === "true" ? true : false;

  // Create a "favorite" state, default value : false
const [isFavorite, setFavorite] = useState(currentIsFavoriteValue)

  // Create function toggleFavorite which toggle from one value to the other (true/false)
const toggleFavorite = () => {
  //Declare new variable which gets opposite value of isFavorite
  const newIsFavoriteValue = !isFavorite
  setFavorite(newIsFavoriteValue);

  localStorage.setItem("FavoriteState", newIsFavoriteValue)
};

// Save the value of isFavorite into localStorage with setItem


  return (
    <div className="MovieItem">
      <h2>{title}</h2>
      <img src={poster} alt={title} />
      <h4>Director: {director}</h4>
      <h5>Released: {released}</h5>
      {/* Ajout d'une action onClick qui appelle la fonction toggleFavorite */}
      <button onClick={() => toggleFavorite()} type="button">{isFavorite ? "Remove from favorites" : "Add to favorites"}</button>
      {/* Changement d'état de la variable isFavortie sans passer par une focntion */}
      {/* <button onClick={() => setFavorite(!isFavorite)} type="button">{isFavorite ? "Remove from favorites" : "Add to favorites"}</button> */}
    </div>
  );
}

MovieItem.propTypes = {
  title: PropTypes.string.isRequired,
  released: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
};

export default MovieItem;
